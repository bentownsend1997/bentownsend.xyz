// Initialize Cloud Firestore through Firebase
firebase.initializeApp({
    apiKey: 'AIzaSyAXdrVLOGmdH-jRJ8OSRYo5vfqoEj-oXjI',
    authDomain: 'bentownsendxyz.firebaseapp.com',
    projectId: 'bentownsendxyz',
    databaseURL: "https://bentownsendxyz.firebaseio.com",
    storageBucket: "bentownsendxyz.appspot.com"
});

var db = firebase.firestore();
console.log("Firebase firestore connection established");

var storage = firebase.storage();
console.log("Firebase storage connection established")

const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

var count = 0;
db.collection("experience")
    .orderBy("start_date", "desc")
    .where("freelance", "==", false)
    .get().then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
            //console.log("\n");
            //console.log("Payload: ", `${doc.id} => ${doc.data()}`);
            //console.log("Name: ", doc.data().name);
            //console.log("Post: ", doc.data().post);
            //console.log("Link: ", doc.data().link);
            var start_date = new Date(doc.data().start_date.seconds * 1000);
            //start_date = start_date.toLocaleDateString("en-GB");
            start_month = monthNames[start_date.getMonth()];
            start_year = start_date.getFullYear();
            start_date = start_month + " " + start_year;

            var end_date;
            if (doc.data().end_date) {
                end_date = new Date(doc.data().end_date.seconds * 1000);
                //end_date = end_date.toLocaleDateString("en-GB");
                end_month = monthNames[end_date.getMonth()];
                end_year = end_date.getFullYear();
                end_date = end_month + " " + end_year;
            } else {
                end_date = "Present";
            }

            const app = document.getElementById('experience');
            const card = document.createElement('div');
            card.setAttribute('class', 'card mb-3');
            const row = document.createElement('div');
            row.setAttribute('class', 'row n-gutters');
            const col1 = document.createElement('div');
            col1.setAttribute('class', 'col-4 experience');

            const card_image = document.createElement('img');
            card_image.setAttribute('src', doc.data().image);
            card_image.setAttribute('class', "card-img");
            card_image.setAttribute('alt', doc.data().name);
            col1.appendChild(card_image);
            row.appendChild(col1);

            const col2 = document.createElement('div');
            col2.setAttribute('class', 'col-8');
            const card_body = document.createElement('div');
            card_body.setAttribute('class', 'card-body');

            const card_title = document.createElement('h5');
            card_title.setAttribute('class', 'card-title');
            card_title.textContent = doc.data().name;
            card_body.appendChild(card_title);

            const card_text = document.createElement('p');
            card_text.setAttribute('class', 'card-text');
            card_text.textContent = doc.data().description;
            card_body.appendChild(card_text);

            const card_subtext = document.createElement('p');
            card_subtext.setAttribute('class', 'card-text');
            const card_subtext_small = document.createElement('small');
            card_subtext_small.setAttribute('class', 'text-muted');
            card_subtext_small.textContent = start_date + " - " + end_date;
            card_subtext.appendChild(card_subtext_small);
            card_body.appendChild(card_subtext);

            col2.appendChild(card_body);

            row.appendChild(col2);

            card.appendChild(row);

            app.appendChild(card);

            const loading = document.getElementById('loading');
            if (loading)
                app.removeChild(loading);

            count++;
        });
        console.log(count + " experiences loaded successfully");
    });

var count2 = 0;
db.collection("experience")
    .orderBy("start_date", "desc")
    .where("freelance", "==", true)
    .get().then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
            //console.log("\n");
            //console.log("Payload: ", `${doc.id} => ${doc.data()}`);
            //console.log("Name: ", doc.data().name);
            //console.log("Post: ", doc.data().post);
            //console.log("Link: ", doc.data().link);
            var start_date = new Date(doc.data().start_date.seconds * 1000);
            //start_date = start_date.toLocaleDateString("en-GB");
            start_month = monthNames[start_date.getMonth()];
            start_year = start_date.getFullYear();
            start_date = start_month + " " + start_year;

            var end_date;
            if (doc.data().end_date) {
                end_date = new Date(doc.data().end_date.seconds * 1000);
                //end_date = end_date.toLocaleDateString("en-GB");
                end_month = monthNames[end_date.getMonth()];
                end_year = end_date.getFullYear();
                end_date = end_month + " " + end_year;
            } else {
                end_date = "Present";
            }

            const app = document.getElementById('freelance');

            const row = document.createElement('div');
            row.setAttribute('class', 'row no-gutters');
            const col1 = document.createElement('div');
            col1.setAttribute('class', 'col-4 experience');
            const card_image = document.createElement('img');
            card_image.setAttribute('src', doc.data().image);
            card_image.setAttribute('class', "card-img");
            card_image.setAttribute('alt', doc.data().name);
            col1.appendChild(card_image);

            row.appendChild(col1);

            const col2 = document.createElement('div');
            col2.setAttribute('class', 'col-8');
            const card_body = document.createElement('div');
            card_body.setAttribute('class', 'card-body');

            const card_title = document.createElement('h5');
            card_title.setAttribute('class', 'card-title');
            card_title.textContent = doc.data().name;
            card_body.appendChild(card_title);

            const card_text = document.createElement('p');
            card_text.setAttribute('class', 'card-text');
            card_text.textContent = doc.data().description;
            card_body.appendChild(card_text);

            const card_subtext = document.createElement('p');
            card_subtext.setAttribute('class', 'card-text');
            const card_subtext_small = document.createElement('small');
            card_subtext_small.setAttribute('class', 'text-muted');
            card_subtext_small.textContent = start_date + " - " + end_date;
            card_subtext.appendChild(card_subtext_small);
            card_body.appendChild(card_subtext);

            col2.appendChild(card_body);

            row.appendChild(col2);

            app.appendChild(row);
            app.appendChild(document.createElement('hr'));

            const loading2 = document.getElementById('loading2');
            if (loading2)
                app.removeChild(loading2);

            count2++;
        });
        console.log(count2 + " clients loaded successfully");
        const app = document.getElementById('freelance');
        
        const card_subtext = document.createElement('p');
        card_subtext.setAttribute('class', 'card-text text-center paddingexp');
        const card_subtext_small = document.createElement('small');
        card_subtext_small.setAttribute('class', 'text-muted');
        card_subtext_small.textContent = "Operating as a sole trader under my own name";
        card_subtext.appendChild(card_subtext_small);
        app.appendChild(card_subtext);
    });