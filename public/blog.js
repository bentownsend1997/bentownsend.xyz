// Initialize Cloud Firestore through Firebase
firebase.initializeApp({
    apiKey: 'AIzaSyAXdrVLOGmdH-jRJ8OSRYo5vfqoEj-oXjI',
    authDomain: 'bentownsendxyz.firebaseapp.com',
    projectId: 'bentownsendxyz',
    databaseURL: "https://bentownsendxyz.firebaseio.com",
    storageBucket: "bentownsendxyz.appspot.com"
});

var db = firebase.firestore();
console.log("Firebase firestore connection established");

var storage = firebase.storage();
console.log("Firebase storage connection established")

var count = 0;
db.collection("blog")
    .orderBy("date", "desc")
    .get().then((querySnapshot) => {
        querySnapshot.forEach((doc) => {
            //console.log("\n");
            //console.log("Payload: ", `${doc.id} => ${doc.data()}`);
            //console.log("Name: ", doc.data().name);
            //console.log("Post: ", doc.data().post);
            //console.log("Link: ", doc.data().link);
            var newDate = new Date(doc.data().date.seconds * 1000);
            var upcoming = false;
            timeAgo = timeSince(new Date(newDate));
            if(newDate > new Date(Date.now())){
                timeAgo = "Upcoming";
                upcoming = true;
            }
            newDate = newDate.toLocaleDateString("en-GB");
            //console.log("Date: ", newDate);

            const app = document.getElementById('root');
            const loading = document.getElementById('loading');
            const card = document.createElement('div');
            const card_image = document.createElement('img');
            const card_video = document.createElement('video');
            const card_body = document.createElement('div');
            const title = document.createElement('h5');
            const post = document.createElement('p');
            const dateText = document.createElement('p');
            const small = document.createElement('small');

            if(doc.data().media){
                if(doc.data().image){
                    card_video.setAttribute('poster', doc.data().image);
                }
                else if ((doc.data().media_format).includes('audio')){
                    card_video.setAttribute('height','60px');
                }
                card_video.setAttribute('controls', '');
                const source = document.createElement('source');
                source.setAttribute('src', doc.data().media);
                source.setAttribute('type', doc.data().media_format);
                card_video.appendChild(source);
                card_video.setAttribute('class', "card-img-top");
                card.appendChild(card_video)
            }
            else if (doc.data().image) {
                card_image.setAttribute('src', doc.data().image);
                card_image.setAttribute('class', "card-img-top");
                card_image.setAttribute('alt', doc.data().name);
                card.appendChild(card_image);
            }
            else if(doc.data().embed){
                card.innerHTML += doc.data().embed;
            }

            title.setAttribute('class', 'card-title');
            title.textContent = doc.data().name;
            card_body.appendChild(title);

            post.setAttribute('class', 'card-text mb-1');
            post.textContent = doc.data().post;
            card_body.appendChild(post);

            if(doc.data().link){
                card_body.innerHTML += doc.data().link;
            }
            
            if(upcoming){
                small.setAttribute('class', 'text-white');
            }
            else{
                small.setAttribute('class', 'text-muted');
            }

            small.textContent = timeAgo + " - " + newDate;
            dateText.appendChild(small);
            dateText.setAttribute('class', 'card-text');
            card_body.appendChild(dateText);

            card_body.setAttribute('class', 'card-body');
            card.appendChild(card_body);

            if(upcoming){
                card.setAttribute('class', 'card text-white bg-secondary mb-3');
            }
            else{
                card.setAttribute('class', 'card mb-3');
            }
            
            app.appendChild(card);

            if (loading)
                app.removeChild(loading);

            count++;
        });
        console.log(count + " blog entries loaded successfully");
    });

    function timeSince(date) {

        var seconds = Math.floor((new Date() - date) / 1000);
      
        var interval = Math.floor(seconds / 31536000);
      
        if (interval > 1) {
          return interval + " years ago";
        }
        interval = Math.floor(seconds / 2592000);
        if (interval > 1) {
          return interval + " months ago";
        }
        interval = Math.floor(seconds / 86400);
        if (interval > 1) {
          return interval + " days ago";
        }
        interval = Math.floor(seconds / 3600);
        if (interval > 1) {
          return interval + " hours ago";
        }
        interval = Math.floor(seconds / 60);
        if (interval > 1) {
          return interval + " minutes ago";
        }
        return Math.floor(seconds) + " seconds ago";
      }